import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiServicesProvider } from '../../providers/api-services/api-services';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loader: Loading;
  credentialsForm: FormGroup;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private storage: Storage,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public apiservice: ApiServicesProvider
    ) {

    this.credentialsForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  onSignIn(){
    this.showLoading();
    
    let form = this.credentialsForm.value;
    this.apiservice.postApi("/users/login", form).subscribe(
      (res) => {
        console.log('Response: ', res);
        if(res.status == 200){
          let body = JSON.parse(res._body);
          this.storage.set('users', body);
          this.navCtrl.setRoot('TabsPage');
        }else{
          this.loader.dismiss();
          let message = 'Mohon periksa kembali username dan password anda.';
          this.showAlert(message);          
        }
      },
      (err) => {
        console.log('Error: ', err);
        let message = 'Server error !!!';
        this.showAlert(message);
      }
    );
  }

  showAlert(message: string = ''){
    let alert = this.alertCtrl.create({
      title: 'Log In',
      message: message,
      buttons: ['OK']
    });

    alert.present();
  }

  showLoading(){
    this.loader = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: 'Mohon tunggu...'
    });
    this.loader.present();
  }

}
