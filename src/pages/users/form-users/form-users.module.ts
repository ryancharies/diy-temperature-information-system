import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormUsersPage } from './form-users';

@NgModule({
  declarations: [
    FormUsersPage,
  ],
  imports: [
    IonicPageModule.forChild(FormUsersPage),
  ],
})
export class FormUsersPageModule {}
