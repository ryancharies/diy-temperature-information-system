import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiServicesProvider } from '../../../providers/api-services/api-services';

/**
 * Generated class for the FormUsersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form-users',
  templateUrl: 'form-users.html',
})
export class FormUsersPage {
  
  id:any = 0;
  loader: Loading;
  formUser: FormGroup;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private apiservice: ApiServicesProvider
    ) {
    
      this.formUser = this.formBuilder.group({
        nama: ['', Validators.required],
        id_akses: ['', Validators.required],
        username: ['', Validators.required],
        password: ['', Validators.required]
      });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormUsersPage');
  }

  onSimpan(){
    this.showLoading();

    let data = this.formUser.value;
    this.apiservice.postApi("users/register",data).subscribe(
      (res) => {
        console.log("Res: ", res);
        if(res.status == 200){
          this.showAlert("Data berhasil disimpan.");
          this.navCtrl.pop();
        }
        this.loader.dismiss();
      },
      (err) => {
        console.log("Err: ", err);
        this.showAlert("Server error !!!");
        this.loader.dismiss();
      }
    );
  }

  showAlert(message: string = '') {
    let alert = this.alertCtrl.create({
      title: 'Form User',
      message: message,
      buttons: ['OK']
    });

    alert.present();
  }

  showLoading() {
    this.loader = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: 'Mohon tunggu...'
    });
    this.loader.present();
  }

}
