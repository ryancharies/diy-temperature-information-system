import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { ApiServicesProvider } from '../../providers/api-services/api-services';

/**
 * Generated class for the UsersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-users',
  templateUrl: 'users.html',
})
export class UsersPage {

  loader: Loading;
  users:any = [];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,    
    public apiservice: ApiServicesProvider
    ) {
  }

  ionViewDidLoad() {
    this.getDataUsers();
  }

  changePage(page: string, user:any = {}){
    this.navCtrl.push(page, {user: user});
  }

  getDataUsers(id:any = 0){
    let data = {id: id};
    this.apiservice.postApi("users/data", data).subscribe(
      (res) => {
        console.log("Res: ", res);
        let body = JSON.parse(res._body);
        this.users = body;
      },
      (err) => {
        console.log("Err: ", err);
      }
    );
  }

  onHapus(id: any){
    this.showConfirm(id);
  }

  setHapus(id){
    this.showLoading();
    this.apiservice.deleteApi("users/"+id).subscribe(
      (res) => {
        console.log('Res: ', res);
        this.showAlert('Data berhasil di hapus');
        this.loader.dismiss();
        this.getDataUsers();
      },
      (err) => {
        console.log('Err: ', err);
        this.showAlert('Server error !!!');
        this.loader.dismiss();
      }
    );
  }

  showAlert(message: string = '') {
    let alert = this.alertCtrl.create({
      title: 'User',
      message: message,
      buttons: ['OK']
    });

    alert.present();
  }  

  showConfirm(id) {
    let alert = this.alertCtrl.create({
      title: 'User',
      message: "Apakah anda ingin menghapus data ini ?",
      buttons: [
        {
          text: 'Tidak',
          handler: () => {

          }
        },
        {
          text: 'Ya, hapus',
          handler: () => {
            this.setHapus(id);
          }
        }
      ]
    });

    alert.present();
  }

  showLoading() {
    this.loader = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: 'Mohon tunggu...'
    });
    this.loader.present();
  }  

}
