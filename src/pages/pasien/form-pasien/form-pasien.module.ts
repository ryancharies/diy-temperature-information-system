import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormPasienPage } from './form-pasien';

@NgModule({
  declarations: [
    FormPasienPage,
  ],
  imports: [
    IonicPageModule.forChild(FormPasienPage),
  ],
})
export class FormPasienPageModule {}
