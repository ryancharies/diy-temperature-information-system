import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiServicesProvider } from '../../../providers/api-services/api-services';
import * as moment from 'moment';
import { Chart } from 'chart.js';
/**
 * Generated class for the FormPasienPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form-pasien',
  templateUrl: 'form-pasien.html',
})
export class FormPasienPage {

  @ViewChild('lineCanvas1') lineCanvas1;
  @ViewChild('lineCanvas2') lineCanvas2;
  @ViewChild('lineCanvas3') lineCanvas3;

  lineChart1: any;
  lineChart2: any;
  lineChart3: any;

  sr: any = [];
  st: any = [];
  k: any = [];
  t: any = [];

  id: any = 0;
  loader: Loading;
  formPasien: FormGroup;
  segment: any = 'biodata';
  isEdit: boolean = false;
  pasien: any = {
    id: '',
    nama: '',
    tanggal_lahir: '',
    tanggal_masuk: '',
    jam_masuk: '',
    tanggal_keluar: '',
    jam_keluar: '',
    usia: '',
    jenis_kelamin: '',
    riwayat_penyakit: '',
  };

  dataSensor: any = [];
  dataCatatan: any = [];
  dataLaporan: any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private apiservice: ApiServicesProvider
  ) {

    let dateNow = moment().format('YYYY-MM-DD');
    let timeNow = moment().format('HH:mm');

    this.formPasien = this.formBuilder.group({
      nama: ['', Validators.required],
      tanggal_lahir: ['', Validators.required],
      tanggal_masuk: ['', Validators.required],
      jam_masuk: ['', Validators.required],
      tanggal_keluar: [''],
      jam_keluar: [''],
      usia: ['', Validators.required],
      jenis_kelamin: ['', Validators.required],
      riwayat_penyakit: ['', Validators.required]
    });

    this.formPasien.patchValue({
      tanggal_masuk: dateNow,
      jam_masuk: timeNow
    });

    let x = navParams.get('pasien');
    if (x != '') {
      this.isEdit = true;
      this.pasien = x;
      this.formPasien.patchValue(x);
      this.getData(this.pasien.id);
    }

  }

  ionViewDidLoad() {
    setInterval(() => {
      this.getData(this.pasien.id);
      this.genChart(this.sr, this.st, this.k, this.t);
    }, 10000);

    setTimeout(() => {
      this.genChart(this.sr, this.st, this.k, this.t);
    }, 5000);

  }

  genChart(sr: any, st: any, k: any, t: any){
    this.lineChart1 = new Chart(this.lineCanvas1.nativeElement, {

      type: 'line',
      data: {
        labels: t,
        datasets: [
          {
            label: "Suhu Ruang",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: sr,
            spanGaps: false,
          }
        ]
      }

    });       

    this.lineChart2 = new Chart(this.lineCanvas2.nativeElement, {

      type: 'line',
      data: {
        labels: t,
        datasets: [
          {
            label: "Suhu Tubuh",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: st,
            spanGaps: false,
          }
        ]
      }

    });     

    this.lineChart3 = new Chart(this.lineCanvas3.nativeElement, {

      type: 'line',
      data: {
        labels: t,
        datasets: [
          {
            label: "Kelembapan",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: k,
            spanGaps: false,
          }
        ]
      }

    });       
  }

  onSimpan() {
    this.showLoading();

    let data = this.formPasien.value;
    data.id = this.pasien.id;
    this.apiservice.postApi("pasien/register", data).subscribe(
      (res) => {
        console.log("Res: ", res);
        if (res.status == 200) {
          this.showAlert("Data berhasil disimpan.");
          this.navCtrl.pop();
        }
        this.loader.dismiss();
      },
      (err) => {
        console.log("Err: ", err);
        this.showAlert("Server error !!!");
        this.loader.dismiss();
      }
    );
  }

  showAlert(message: string = '') {
    let alert = this.alertCtrl.create({
      title: 'Form User',
      message: message,
      buttons: ['OK']
    });

    alert.present();
  }

  showLoading() {
    this.loader = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: 'Mohon tunggu...'
    });
    this.loader.present();
  }

  getData(id: any){
    this.apiservice.getApi("pasien/catatan/"+id).subscribe(
      (res) => {
        console.log("Catatan: ", res);
        this.dataCatatan = res;
      },
      (err) => {
        console.log("Catatan error: ", err);
      }
    );

    this.apiservice.getApi("pasien/laporan/"+id).subscribe(
      (res) => {
        console.log("Laporan: ", res);
        this.dataLaporan = res;
      },
      (err) => {
        console.log("Laporan error: ", err);
      }
    );

    this.apiservice.getApi("pasien/sensor/"+id).subscribe(
      (res) => {
        console.log("Sensor: ", res);
        this.dataSensor = res;
      },
      (err) => {
        console.log("Sensor error: ", err);
      }
    );

    this.apiservice.getApi("pasien/sensor/chart/"+id).subscribe(
      (res) => {
        console.log("Chart: ", res);
        this.sr = res.sr;
        this.st = res.st;
        this.k = res.k;
        this.t = res.t;
      },
      (err) => {
        console.log("Chart error: ", err);
      }
    );
  }

}
