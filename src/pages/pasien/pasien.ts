import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { ApiServicesProvider } from '../../providers/api-services/api-services';

/**
 * Generated class for the PasienPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pasien',
  templateUrl: 'pasien.html',
})
export class PasienPage {

  pasiens: any = [];
  loader: Loading;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public apiservice: ApiServicesProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PasienPage');
  }

  ionViewWillEnter() {
    this.getDataPasien();
  }

  changePage(page: string, pasien: any = '') {
    this.navCtrl.push(page, {pasien: pasien});
  }

  getDataPasien() {
    this.apiservice.getApi("pasien/data").subscribe(
      (res) => {
        console.log("Res: ", res);
        // let body = JSON.parse(res._body);
        this.pasiens = res;
      },
      (err) => {
        console.log("Err: ", err);
      }
    );
  }

  onHapus(id: any) {
    this.showConfirm(id);
  }

  setHapus(id) {
    this.showLoading();
    this.apiservice.deleteApi("pasien/" + id).subscribe(
      (res) => {
        console.log('Res: ', res);
        this.showAlert('Data berhasil di hapus');
        this.loader.dismiss();
        this.getDataPasien();
      },
      (err) => {
        console.log('Err: ', err);
        this.showAlert('Server error !!!');
        this.loader.dismiss();
      }
    );
  }

  showAlert(message: string = '') {
    let alert = this.alertCtrl.create({
      title: 'Pasien',
      message: message,
      buttons: ['OK']
    });

    alert.present();
  }

  showConfirm(id) {
    let alert = this.alertCtrl.create({
      title: 'Pasien',
      message: "Apakah anda ingin menghapus data ini ?",
      buttons: [
        {
          text: 'Tidak',
          handler: () => {

          }
        },
        {
          text: 'Ya, hapus',
          handler: () => {
            this.setHapus(id);
          }
        }
      ]
    });

    alert.present();
  }

  showLoading() {
    this.loader = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: 'Mohon tunggu...'
    });
    this.loader.present();
  }  

}
