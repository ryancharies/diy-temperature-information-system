import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, App, Loading, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the PengaturanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pengaturan',
  templateUrl: 'pengaturan.html',
})
export class PengaturanPage {

  loader: Loading;
  admin: boolean = false;

  constructor(
    public app: App,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private storage: Storage
    ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PengaturanPage');
    this.storage.get('users').then(
      (val) => {
        console.log(val);
        if(val.id_akses == 0)
          this.admin = true;
      }
    );
  }

  changePage(page: string){
    this.navCtrl.push(page);
  }

  onLogout(){
    this.showDialogLogOut();
  }

  showDialogLogOut(){
    let confirm = this.alertCtrl.create({
      title: 'Log Out',
      message: 'Apakah anda ingin keluar dari aplikasi ?',
      buttons: [
        {
          text: 'Tidak',
          handler: () => {
            console.log('Tidak clicked');            
          }
        },
        {
          text: 'Ya',
          handler: () => {
            this.showLoading();
            this.storage.set('users', null);
            setTimeout(() => {
              this.app.getRootNav().setRoot('LoginPage');
            }, 2000);
          }
        }
      ]
    });

    confirm.present();
  }

  showLoading() {
    this.loader = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: 'Mohon tunggu...'
    });
    this.loader.present();
  }  

}
