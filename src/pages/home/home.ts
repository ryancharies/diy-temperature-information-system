import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ApiServicesProvider } from '../../providers/api-services/api-services';
import { LocalNotifications } from '@ionic-native/local-notifications';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  segment: any = 'alat1';
  sensor: any = {
    suhu_ruang: 0,
    suhu_tubuh: 0,
    kelembapan: 0
  };
  sensor_kedua: any = {
    suhu_ruang: 0,
    suhu_tubuh: 0,
    kelembapan: 0
  };
  pasien: any = 1;
  pasien_kedua: any = 2;
  users: any = {};
  pasiens: any = [];
  catatan: string = '';
  catatan_kedua: string = '';
  laporan: string = '';
  laporan_kedua: string = '';

  constructor(
    public navCtrl: NavController,
    public storage: Storage,
    public alertCtrl: AlertController,
    public apiservice: ApiServicesProvider,
    private localNotifications: LocalNotifications
    // public notif: LocalNotificationsOriginal
  ) {
    this.getDataSensor();
  }

  ionViewWillEnter(){
    this.getDataSensor();    
    this.getDataSensorKedua();    
    this.getDataPasien();
    this.getPasienAktif(); 
    this.getPasienAktifKedua(); 
  }

  ionViewDidLoad(){ 

    this.getDataUsers();
    this.getDataPasien();
    setInterval(() => {
      this.getDataSensor(); 
      this.getDataSensorKedua(); 
    }, 10000);      
  }

  getDataPasien(){
    this.apiservice.getApi("pasien/datapasienmasuk").subscribe(
      (res) => {
        console.log("Res: ", res);
        this.pasiens = res;
      },
      (err) => {
        console.log("Err: ", err);
      }
    );
  }

  getDataUsers(){
    this.storage.get('users').then(
      (val) => {
        console.log('Storage users: ', val);
        this.users = val;
      }
    )
  }

  getPasienAktif(){
    this.apiservice.getApi("pasien/aktif").subscribe(
      (res) => {
        console.log("Res: ", res);
        this.pasien = res;
      },
      (err) => {
        console.log("Err: ", err);
      }
    );
  }

  getPasienAktifKedua(){
    this.apiservice.getApi("pasien/aktifkedua").subscribe(
      (res) => {
        console.log("Res: ", res);
        this.pasien_kedua = res;
      },
      (err) => {
        console.log("Err: ", err);
      }
    );
  }

  getDataSensor(){
    this.apiservice.getApi("pasien/sensor/data/"+this.pasien+"/1").subscribe(
      (res) => {
        console.log("Sensor: ", res);
        this.sensor = res;
        
        if(res.suhu_ruang < 22){
          this.showNotif('Suhu ruangan terlalu dingin !!!');
        }else if(res.suhu_ruang > 24){
          this.showNotif('Suhu ruangan terlalu panas !!!');
        }

        if(res.kelembapan < 45){
          this.showNotif('Ruangan terlalu kering !!!');
        }else if(res.kelembapan > 60){
          this.showNotif('Ruangan terlalu lembab !!!');
        }


        if(res.suhu_tubuh < 36){
          this.showNotif('Pasien pada alat 1 mengalami hipotermia !!!');
        }

      },
      (err) => {
        console.log("Sensor err: ", err);
      }
    );
  }

  getDataSensorKedua(){
    this.apiservice.getApi("pasien/sensor/data/"+this.pasien_kedua+"/2").subscribe(
      (res) => {
        console.log("Sensor: ", res);
        this.sensor_kedua = res;

        if(res.suhu_tubuh < 36){
          this.showNotif('Pasien pada alat 2 mengalami hipotermia !!!');
        }
        
      },
      (err) => {
        console.log("Sensor err: ", err);
      }
    );
  }

  gantiPasien(no: any, ev: any){
    let data = {no: no, id: ev};
    this.apiservice.putApi("pasien/ganti",data).subscribe(
      (res) => {
        console.log("Res: ", res);
        
      },
      (err) => {
        console.log("Err: ", err);
      }
    );
  }

  showNotif(msg: any){
    this.localNotifications.schedule({
      text: msg,
      led: 'FF0000',
      trigger: {at: new Date(new Date().getTime() + 3600)},
      sound: 'file://assets/sounds/plucky.mp3'
   });   
  }

  showAlert(message: string = '') {
    let alert = this.alertCtrl.create({
      title: 'Catatan',
      message: message,
      buttons: ['OK']
    });

    alert.present();
  }

  onSimpan(){
    let data = {
      id_pasien: this.pasien,
      id_users: this.users.id,
      id_ruangan: 1,
      catatan: this.catatan,
      laporan: this.laporan
    }

    this.apiservice.postApi("pasien/catatan", data).subscribe(
      (res) => {
        console.log("Res: ", res);
        this.catatan = "";  
        this.laporan = "";  
        this.showAlert("Catatan berhasil diinput.");
      },
      (err) => {
        console.log("Err: ", err);
        this.showAlert("Server error !!!");
      }
    );
  }

  onSimpanKedua(){
    let data = {
      id_pasien: this.pasien_kedua,
      id_users: this.users.id,
      id_ruangan: 1,
      catatan: this.catatan_kedua,
      laporan: this.laporan_kedua
    }

    this.apiservice.postApi("pasien/catatan", data).subscribe(
      (res) => {
        console.log("Res: ", res);
        this.catatan_kedua = "";  
        this.laporan_kedua = "";  
        this.showAlert("Catatan berhasil diinput.");
      },
      (err) => {
        console.log("Err: ", err);
        this.showAlert("Server error !!!");
      }
    );
  }

}
