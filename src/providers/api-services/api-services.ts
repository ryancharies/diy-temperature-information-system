import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

/*
  Generated class for the ApiServicesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiServicesProvider {

    private baseUrl: string = 'http://192.168.0.100/apilumenrev2/api/';
    // private baseUrl: string = 'http://127.0.0.1/trial/apiTemperatureInformation/api/';
    // private baseUrl: string = 'http://192.168.1.186/trial/apiTemperatureInformation/api/';

    constructor(public http: Http) {
      console.log('Hello ApiserviceProvider Provider');
    }

    setUrl(param: string) {
      // return this.baseUrl + param + this.key;
      return this.baseUrl + param;
    }

    getApi(param: string) {
      let url = this.setUrl(param);
      return this.http.get(url)
        .do((res: Response) => this.logResponse(res))
        .map((res: Response) => this.extractData(res))
        .catch((error: Response) => this.catchError(error));
    }

    postApi(param: string, data: any) {
      let url = this.setUrl(param);
      return this.http.post(url, JSON.stringify(data))
        .do((res: Response) => this.logResponse(res))
        .catch((error: Response) => this.catchError(error));
    }

    putApi(param: string, data: any) {
      let url = this.setUrl(param);
      return this.http.put(url, JSON.stringify(data))
        .do((res: Response) => this.logResponse(res))
        .catch((error: Response) => this.catchError(error));
    }

    deleteApi(param: string) {
      let url = this.setUrl(param);
      return this.http.delete(url)
        .do((res: Response) => this.logResponse(res))
        .catch((error: Response) => this.catchError(error));
    }

    private catchError(error: Response | any) {
      console.log(error);
      return Observable.throw(error.json().error || 'Server Error');
    }

    private logResponse(res: Response) {
      console.log(res);
    }

    private extractData(res: Response) {
      return res.json();
    }



}
