import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage: any;

  constructor(private platform: Platform, private statusBar: StatusBar, private splashScreen: SplashScreen, public storage: Storage) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.rootPage = 'TabsPage';

      this.statusBar.styleBlackTranslucent();
      // this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.storage.get('users').then((val) => {
        console.log('Auth: ', val);
        let isLogin = false;
        if (val != null)
          isLogin = true;
        this.rootPage = (isLogin) ? 'TabsPage' : 'LoginPage';
      });

    });
  }

}
